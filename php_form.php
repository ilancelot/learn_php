<?php
/*
通过今天的这个实例我们来实现基本、安全的 “PHP表单提交”
我使用的编辑器是Atom(家里的电脑配置高一些，可以使用这坨了),使用PHPStorm测试就可以.
Atom : http://atom.io/
PHPStorm : http://www.jetbrains.com/phpstorm/
------------------------------------------------------------------------------
1.简介
PHP有两个超全局变量: $_GET 和 $_POST 用来收集表单的数据，分别对应提交表单时的GET方法和POST方法，
在PHP中，GET和POST都是创建“数组”。包含键值对(KEY=>VALUE),键(KEY)是表单控件的名称，值(VALUE)是来自用户的输入数据
由于 $_GET 和 $_POST 是超全局变量，所以你可以无需任何代码，在任何地方方便的访问他们。
2.比较
GET
通过 GET 方法从表单发送的信息对任何人都是可见的（所有变量名和值都显示在 URL 中）。GET 对所发送信息的数量也有限制。
限制在大于 2000 个字符。不过，由于变量显示在 URL 中，把页面添加到书签中也更为方便。
POST
通过 POST 方法从表单发送的信息对其他人是不可见的（所有名称/值会被嵌入 HTTP 请求的主体中），并且对所发送信息的数量也无限制。
此外 POST 支持高阶功能，比如在向服务器上传文件时进行 multi-part 二进制输入。
不过，由于变量未显示在 URL 中，也就无法将页面添加到书签。
*偏爱POST*
-------------------------------------------------------------------------------
处理表单的时候一定要注意的就是 “安全性”
安全处理HTML表单极其重要。
-------------------------------------------------------------------------------
本例是一个安全提交表单的实例，接下来的知识将会通过代码中的 注释 来体现。
祝学习成功！
Lancelot Stark
2015/7/9
*/
 ?>

 <?php
 // 定义变量并设置为空值，PHP可以“连等”
 $nameErr = $emailErr = $genderErr = $websiteErr = "";//定义错误信息，默认为空
 $name = $email = $gender = $comment = $website = "";//定义结果数据，默认为空

 if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
      $nameErr = "姓名是必填的";
    } else {
      $name = test_input($_POST["name"]);
      // 检查姓名是否包含字母和空白字符
      //preg_match()正则表达式，看起来像是一坨屎，其实它就是一坨屎。如果不会的话也不用掌握，因为这些有人帮你造好了“轮子”，直接抄袭过来用对新手来说也行。
      if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
        $nameErr = "只允许字母和空格";
      }
    }

    if (empty($_POST["email"])) {
      $emailErr = "电邮是必填的";
    } else {
      $email = test_input($_POST["email"]);
      // 检查电子邮件地址语法是否有效
      if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email)) {
        $emailErr = "无效的 email 格式";
      }
    }

    if (empty($_POST["website"])) {
      $website = "";
    } else {
      $website = test_input($_POST["website"]);
      // 检查 URL 地址语法是否有效（正则表达式也允许 URL 中的斜杠）
      if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
        $websiteErr = "无效的 URL";
      }
    }

    if (empty($_POST["comment"])) {
      $comment = "";
    } else {
      $comment = test_input($_POST["comment"]);
    }

    if (empty($_POST["gender"])) {
      $genderErr = "性别是必选的";
    } else {
      $gender = test_input($_POST["gender"]);
    }
 }

 function test_input($data) {
   //用三个PHP自带函数处理提交的数据i，以下三条最后网址是官方文档
    $data = trim($data);//去掉收尾空白字符 http://php.net/manual/zh/function.trim.php
    $data = stripslashes($data);//返回反转义后的字符串。可识别类似 C 语言的 \n，\r，... 八进制以及十六进制的描述。 http://php.net/manual/zh/function.stripcslashes.php
    $data = htmlspecialchars($data);//将特殊字符转化( <>转换为 &lt; 和 &gt; )，可以防XSS http://php.net/manual/zh/function.htmlspecialchars.php

    return $data;
 }
 ?>


 <!DOCTYPE HTML>
 <html>
 <head>
 <style>
  .error {color: #FF0000;}
 </style>
 </head>
 <body>


 <h2>PHP 验证实例</h2>
 <p><span class="error">* 必输内容</span></p>

 <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <p>姓名：<input type="text" name="name">
    <span class="error">* <?php echo $nameErr;?></span>
    </p>
    <p>
    电邮：<input type="text" name="email">
    <span class="error">* <?php echo $emailErr;?></span>
    </p>
    <p>
    网址：<input type="text" name="website">
    <span class="error"><?php echo $websiteErr;?></span>
    </p>
    <p>
    评论：<textarea name="comment" rows="5" cols="40"></textarea>
    </p>
    <p>
    性别：
    <input type="radio" name="gender" value="female">女性
    <input type="radio" name="gender" value="male">男性
    <span class="error">* <?php echo $genderErr;?></span>
   </p>
    <input type="submit" name="submit" value="提交">
 </form>
 <?php
 echo "<h2>您的输入：</h2>";
 echo $name;
 echo "<br>";
 echo $email;
 echo "<br>";
 echo $website;
 echo "<br>";
 echo $comment;
 echo "<br>";
 echo $gender;
 ?>

 </body>
 </html>
